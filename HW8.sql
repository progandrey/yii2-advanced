-- ## Task 1.1 ##
--INSERT INTO category (name) VALUES ('Honda'), ('Toyota'), ('Mitsu bishi');
--INSERT INTO sub_category (name) VALUES ('Camry'), ('Corolla'), ('Acord');

--SELECT c.name, s.name
--FROM category c
--FULL OUTER JOIN sub_category s
--On c.id = s.id

SELECT name
FROM category
UNION SELECT name
FROM sub_category

-- ## Task 1.2 ##
--INSERT INTO countries (name) VALUES ('Ukraine'), ('Uruguay'), ('Uzbekistan'), ('Venezuela'), ('Zimbabwe'), ('Uganda'), ('Turkmenistan'), ('Turkey'), ('Tunisia'), ('Spain');
--INSERT INTO cities (name, country_id) VALUES ('Kyiv', 1), ('Montevideo', 2), ('Tashkent', 3), ('Caracas', 4), ('Harare', 5), ('Kampala', 6), ('Ashgabat', 7), ('Ankara', 8), ('Tunis', 9), ('Madrid', 10);

-- ## Task 2.2 ##
SELECT name
FROM countries
UNION SELECT name
FROM cities

-- ## Task 2.3 ##
SELECT name
FROM countries
UNION ALL SELECT name
FROM cities

-- ## Task 2.4 ##
SELECT name
FROM countries
WHERE id > 5
UNION ALL SELECT name
FROM cities
WHERE id <= 10

-- ## Task 2.5 ##
SELECT name all_name
FROM countries
WHERE id > 5
UNION ALL SELECT name
FROM cities
WHERE id <= 10

-- ## Task 3.2 ##
INSERT INTO page (name, category_id) VALUES ('p Kyiv', 1), ('p Montevideo', 2), ('p Tashkent', 3), ('p Caracas', 1), ('p Harare', 2),
('p Kampala', 3), ('p Ashgabat', 1), ('p Ankara', 3), ('p Tunis', 2), ('p Madrid', 1);

SELECT c.name c_name, p.name p_name
FROM page p
JOIN category c
On p.category_id = c.id

-- ## Task 4.1 ##
INSERT INTO page (name, category_id) VALUES ('p Kyiv', 1), ('p Montevideo', 2), ('p Tashkent', 3), ('p Caracas', 1), ('p Harare', 2), ('p Kampala', 3),
('p Ashgabat', 1), ('p Ankara', 3), ('p Tunis', 2), ('p Madrid', 1);

-- ## Task 4.2 ##
SELECT c.id c_id, c.name c_name, p.id p_id, p.name p_name, p.category_id p_category_id, sc.id sc_id, sc.name sc_name, sc.category_id sc_category_id
FROM category c, page p, sub_category sc
WHERE c.id = p.category_id

SELECT c.id c_id, c.name c_name, p.id p_id, p.name p_name, p.category_id p_category_id, sc.id sc_id, sc.name sc_name, sc.category_id sc_category_id
FROM category c
LEFT JOIN page p
LEFT JOIN sub_category sc
On c.id = p.category_id AND c.id = sc.category_id

-- ## Task 5.3 ##
--$this->createIndex('index_messages_forum_id', 'messages', 'forum_id');
CREATE INDEX  index2_messages_forum_id
ON messages(message);

EXPLAIN SELECT * FROM messages;

-- ## Task 6.1 ##
CREATE TABLE films (
    id      integer PRIMARY KEY,
    title   varchar(40) NOT NULL,
    len     integer
);

INSERT INTO "films" (id, title, len) VALUES
(1, 'Wars', 223),
(2, 'Titanik', 156),
(3, 'Forsage', 112);

UPDATE films SET title = 'Dramatic' WHERE id = 2;

DROP TABLE films;


