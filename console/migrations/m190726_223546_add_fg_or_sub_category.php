<?php

use yii\db\Migration;

/**
 * Class m190726_223546_add_fg_or_sub_category
 */
class m190726_223546_add_fg_or_sub_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sub_category', 'category_id', $this->integer()->defaultValue(1));

        $this->addForeignKey('FG_category_page', 'sub_category', 'category_id', 'category', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FG_category_page', 'sub_category');

        $this->dropColumn('sub_category', 'category_id');
    }
}
