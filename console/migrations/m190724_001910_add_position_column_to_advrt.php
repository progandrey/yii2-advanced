<?php

use yii\db\Migration;

/**
 * Class m190724_001910_add_position_column_to_advrt
 */
class m190724_001910_add_position_column_to_advrt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('advert', 'img', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('advert', 'img');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_001910_add_position_column_to_advrt cannot be reverted.\n";

        return false;
    }
    */
}
