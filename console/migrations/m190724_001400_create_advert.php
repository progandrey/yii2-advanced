<?php

use yii\db\Migration;

/**
 * Class m190724_001400_create_advert
 */
class m190724_001400_create_advert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('advert', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'autor' => $this->string(),
            'date' => $this->string(),
            'active' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('advert');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_001400_create_advert cannot be reverted.\n";

        return false;
    }
    */
}
