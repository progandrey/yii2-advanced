<?php

use yii\db\Migration;

/**
 * Class m190722_185612_create_items
 */
class m190722_185612_create_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('items', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'content' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('items');
        $this->dropTable('{{%user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function safeUp()
    {

    }

    public function down()
    {
        echo "m190722_185612_create_items cannot be reverted.\n";

        return false;
    }
    */
}
