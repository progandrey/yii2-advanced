<?php

use yii\db\Migration;

/**
 * Class m190725_181628_create_sub_category
 */
class m190725_181628_create_sub_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sub_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        Yii::$app->db->createCommand()->batchInsert('sub_category', ['name'], [
            ['Camry'],
            ['Corolla'],
            ['Acord'],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sub_category');
    }
}
