<?php

use yii\db\Migration;

/**
 * Class m190725_193945_create_countries
 */
class m190725_193945_create_countries extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        Yii::$app->db->createCommand()->batchInsert('countries', ['name'], [
            ['Ukraine'],
            ['Uruguay'],
            ['Uzbekistan'],
            ['Venezuela'],
            ['Zimbabwe'],
            ['Uganda'],
            ['Turkmenistan'],
            ['Turkey'],
            ['Tunisia'],
            ['Spain'],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('countries');
    }
}
