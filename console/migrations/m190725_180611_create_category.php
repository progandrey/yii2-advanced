<?php

use yii\db\Migration;

/**
 * Class m190725_180611_create_category
 */
class m190725_180611_create_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ]);

        Yii::$app->db->createCommand()->batchInsert('category', ['name'], [
            ["Child's world"],
            ['The property'],
            ['Transport'],
            ['Spare parts for transport'],
            ['The robot'],
            ['Animals'],
            ['A house and a garden'],
            ['Electronics'],
            ['Business and services'],
            ['Fashion & Style'],
            ['Give a gift'],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
