<?php

use yii\db\Migration;

/**
 * Class m190725_210920_create_page
 */
class m190725_210920_create_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer()->defaultValue(1),
        ]);

        $this->addForeignKey('FG_category_page', 'page', 'category_id', 'category', 'id', 'CASCADE');

        Yii::$app->db->createCommand()->batchInsert('page', ['name', 'category_id'], [
            ['page 1', 1],
            ['page 2', 2],
            ['page 3', 3],
            ['page 4', 4],
            ['page 5', 5],
            ['page 6', 6],
            ['page 7', 7],
            ['page 8', 8],
            ['page 9', 9],
            ['page 10', 10],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FG_category_page', 'page');
        $this->dropTable('page');
    }
}
