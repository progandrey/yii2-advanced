<?php

use yii\db\Migration;

/**
 * Class m190724_000842_article
 */
class m190724_000842_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'autor' => $this->string(),
            'date' => $this->string(),
            'active' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('article');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_000842_article cannot be reverted.\n";

        return false;
    }
    */
}
