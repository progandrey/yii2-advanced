<?php

use yii\db\Migration;

/**
 * Class m190725_194316_create_cities
 */
class m190725_194316_create_cities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cities', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'country_id' => $this->integer()->notNull(),
        ]);

        Yii::$app->db->createCommand()->batchInsert('cities', ['name', 'country_id'], [
            ['Kyiv', 1],
            ['Montevideo', 2],
            ['Tashkent', 3],
            ['Caracas', 4],
            ['Harare', 5],
            ['Kampala', 6],
            ['Ashgabat', 7],
            ['Ankara', 8],
            ['Tunis', 9],
            ['Madrid', 10],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cities');
    }
}
