<?php

use yii\db\Migration;

/**
 * Class m190726_233731_add_test_table
 */
class m190726_233731_add_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('forums', [
            'id' => $this->primaryKey()->comment('Primary key'),
            'created_time' => $this->integer()->comment('Created time'),
            'updated_time' => $this->integer()->comment('Updated time'),
            'user_create' => $this->integer(),
            'user_update' => $this->integer(),
        ]);
        $this->createTable('messages', [
            'id' => $this->primaryKey()->comment('Primary key'),
            'message' => $this->text()->notNull(),
            'created_time' => $this->integer()->comment('Created time'),
            'updated_time' => $this->integer()->comment('Updated time'),
            'user_to' => $this->integer(),
            'user_from' => $this->integer(),
            'forum_id' => $this->integer(),
        ]);
        $this->addForeignKey('FK_forums_messages', 'messages', 'forum_id', 'forums', 'id', 'CASCADE');
        for ($i = 1; $i < 101; $i++) {
            $userTo = 20000 + $i;
            $userFrom = 20000 - $i;
            Yii::$app->db->createCommand(
                'INSERT INTO "forums" (created_time, updated_time, user_create, user_update) VALUES (:created_time, :updated_time, :user_create, :user_update)',
                [
                    ':created_time' => time(),
                    ':updated_time' => time(),
                    ':user_create' => $userTo,
                    ':user_update' => $userFrom,
                ]
            )
                ->execute();
            $k = 0;
            while ($k < 10) {
                Yii::$app->db->createCommand(
                    'INSERT INTO "messages" (message, created_time, updated_time, user_to, user_from, forum_id) VALUES (:message, :created_time, :updated_time, :user_to, :user_from, :forum_id)',
                    [
                        ':message' => 'Forum message # ' . $k . ' from user ' . $userFrom . ' to user ' . $userTo,
                        ':created_time' => time(),
                        ':updated_time' => time(),
                        ':user_to' => $userTo,
                        ':user_from' => $userFrom,
                        ':forum_id' => $i,
                    ]
                )
                    ->execute();
                $k++;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_forums_messages', 'messages');
        $this->dropTable('forums');
        $this->dropTable('messages');
    }
}
