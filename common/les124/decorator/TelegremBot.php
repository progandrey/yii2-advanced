<?php

namespace common\les124\decorator;

use common\les124\decorator\NotificationInterface as NotificationInterface;

/**
 * Class TelegremBot
 *
 * @package common\les124\decorator
 */
class TelegremBot implements NotificationInterface
{
    public $notification;

    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    public function getMessage(): string
    {
        return $this->notification->getMessage() . ' Telegrem Bot';
    }
}
