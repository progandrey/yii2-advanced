<?php

namespace common\les124\decorator;

use common\les124\decorator\NotificationInterface as NotificationInterface;

/**
 * Class Email
 *
 * @package common\les124\decorator
 */
class Email implements NotificationInterface
{
    public $notification;

    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    public function getMessage(): string
    {
        return $this->notification->getMessage() . ' email';
    }
}
