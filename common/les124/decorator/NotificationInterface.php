<?php

namespace common\les124\decorator;

/**
 * Interface NotificationInterface
 *
 * @package common\les124\decorator
 */
interface NotificationInterface
{
    public function getMessage(): string ;
}
