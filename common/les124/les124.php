<?php

namespace common\les1231\Adaptor;

// 4.2
/*
use common\les124\adaptor\StockExchange as StockExchange;
use common\les124\adaptor\StockExchangeAdaptor as StockExchangeAdaptor;
use common\les124\adaptor\Attachment as Attachment;

$stockXML = new StockExchange();
$stockJson = new StockExchangeAdaptor($stockXML);

$attachment = new Attachment();
$attachment->getQuotes($stockJson);


// 4.3
use common\les124\bridge\WhiteColor as WhiteColor;
use common\les124\bridge\ToyotaCar as ToyotaCar;

$color = new WhiteColor();

$figure = new CubeFugure($color);
$figure->getFigure();

// 4.5
$email = new Email();
$cmc = new CMC($email);
$skypeBot = new SkypeBot($cmc);
$telegramBot = new TelegramBot($skypeBot);
$message = new PhoneCall($telegramBot);

echo $message->getMessage();

// 4.6
use common\les124\facade\OnlineStoreFacade as OnlineStoreFacade46;

$shop46 = new OnlineStoreFacade46();
$shop46->shop();
*/