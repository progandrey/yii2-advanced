<?php

namespace common\les124\bridge;

use common\les124\bridge\FigureInterface as FigureInterface;
use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class BallFugure
 *
 * @package common\les124\bridge
 */
class BallFugure implements FigureInterface
{
    protected $color;

    public function __construct(ColorInterface $color)
    {
        $this->color = $color;
    }

    public function getFigure()
    {
        return "Ball color " . $this->color->getColor();
    }
}
