<?php

namespace common\les124\bridge;

use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Interface FigureInterface
 *
 * @package common\les124\bridge
 */
interface FigureInterface
{
    public function __construct(ColorInterface $color);
    public function getFigure();
}
