<?php

namespace common\les124\bridge;

use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class GreenColor
 *
 * @package common\les124\bridge
 */
class GreenColor implements ColorInterface
{
    /**
     * @return string
     */
    public function getColor(): string
    {
        return 'green';
    }
}
