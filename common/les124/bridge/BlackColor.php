<?php

namespace common\les124\bridge;

use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class BlackColor
 *
 * @package common\les124\bridge
 */
class BlackColor implements ColorInterface
{
    /**
     * @return string
     */
    public function getColor(): string
    {
        return 'black';
    }
}
