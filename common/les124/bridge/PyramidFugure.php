<?php

namespace common\les124\bridge;

use common\les124\bridge\FigureInterface as FigureInterface;
use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class PyramidFugure
 *
 * @package common\les124\bridge
 */
class PyramidFugure implements FigureInterface
{
    protected $color;

    public function __construct(ColorInterface $color)
    {
        $this->color = $color;
    }

    public function getFigure()
    {
        return "Pyramid color " . $this->color->getColor();
    }
}
