<?php

namespace common\les124\bridge;

use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class WhiteColor
 *
 * @package common\les124\bridge
 */
class WhiteColor implements ColorInterface
{
    /**
     * @return string
     */
    public function getColor(): string
    {
        return 'white';
    }
}
