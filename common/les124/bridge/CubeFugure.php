<?php

namespace common\les124\bridge;

use common\les124\bridge\FigureInterface as FigureInterface;
use common\les124\bridge\ColorInterface as ColorInterface;

/**
 * Class CubeFugure
 *
 * @package common\les124\bridge
 */
class CubeFugure implements FigureInterface
{
    protected $color;

    public function __construct(ColorInterface $color)
    {
        $this->color = $color;
    }

    public function getFigure()
    {
        return "Cube color " . $this->color->getColor();
    }
}
