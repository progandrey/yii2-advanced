<?php

namespace common\les124\bridge;

/**
 * Interface ColorInterface
 *
 * @package common\les124\bridge
 */
interface ColorInterface
{
    public function getColor();
}
