<?php

namespace common\les124\adaptor;

use common\les124\adaptor\ApplicationInterface as ApplicationInterface;

/**
 * Class Application
 *
 * @package common\les124\adaptor
 */
class Application implements ApplicationInterface
{
    /**
     * @return bool
     */
    public function getStocksJson(): bool
	{
		return true;
	}
}
