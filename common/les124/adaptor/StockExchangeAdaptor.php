<?php

namespace common\les124\adaptor;

use common\les124\adaptor\ApplicationInterface as ApplicationInterface;
use common\les124\adaptor\StockExchange as StockExchange;

/**
 * Class StockExchangeAdaptor
 *
 * @package common\les124\adaptor
 */
class StockExchangeAdaptor implements ApplicationInterface
{
    private $stockExchange;

    public function __construct(StockExchange $stockExchange)
    {
        $this->stockExchange = $stockExchange;
    }

    public function getStocksJson()
    {
        $this->stockExchange->getStocksXML();
    }
}
