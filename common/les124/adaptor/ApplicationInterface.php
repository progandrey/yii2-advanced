<?php

namespace common\les124\adaptor;

/**
 * Interface ApplicationInterface
 *
 * @package common\les124\adaptor
 */
interface ApplicationInterface
{
    public function getStocksJson();
}
