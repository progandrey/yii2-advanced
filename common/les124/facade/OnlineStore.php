<?php

namespace common\les124\facade;

/**
 * Class OnlineStore
 *
 * @package common\les124\facade
 */
class OnlineStore
{
    public function customerCall()
    {
        echo "звонок клиента";
    }

    public function orderFormation()
    {
        echo "формирование заказа";
    }

    public function payment()
    {
        echo "оплата";
    }

    public function stock()
    {
        echo "склад";
    }

    public function packaging()
    {
        echo "упаковка";
    }

    public function delivery()
    {
        echo "доставка";
    }

    public function gettingToCustomers()
    {
        echo "получение клиентм";
    }
}
