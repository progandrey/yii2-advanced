<?php

namespace common\les124\facade;

use common\les124\facade\OnlineStore as OnlineStore;

/**
 * Class OnlineStoreFacade
 *
 * @package common\les124\facade
 */
class OnlineStoreFacade
{
    private $shop;

    public function __construct(OnlineStore $shop)
    {
        $this->shop = $shop;
    }

    public function shop()
    {
        $this->shop->customerCall();
        $this->shop->orderFormation();
        $this->shop->payment();
        $this->shop->stock();
        $this->shop->packaging();
        $this->shop->delivery();
        $this->shop->gettingToCustomers();
    }
}
