<?php

namespace common\les1231\composite;

/**
 * Interface RenderableInterface
 *
 * @package common\les1231\composite
 */
interface RenderableInterface
{
    public function render(): string;
}
