<?php

namespace common\les1231\composite;

use common\les1231\composite\RenderableInterface as RenderableInterface;

/**
 * Class Wheels
 *
 * @package common\les1231\composite
 */
class Wheels implements RenderableInterface
{
    public function render(): string
    {
        return '<br>Wheels';
    }
}
