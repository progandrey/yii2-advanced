<?php

namespace common\les1231\composite;

use common\les1231\composite\RenderableInterface as RenderableInterface;

/**
 * Class Engine
 *
 * @package common\les1231\composite
 */
class Engine implements RenderableInterface
{
    public function render(): string
    {
        return '<br>Engine';
    }
}
