<?php

namespace common\les1231\composite;

use common\les1231\composite\RenderableInterface as RenderableInterface;

/**
 * Class Car
 *
 * @package common\les1231\composite
 */
class Car implements RenderableInterface
{
    private $elements;

    public function render(): string
    {
        $formCode = '';

        foreach ($this->elements as $element) {
            /**
             * @var $element RenderableInterface
             */
            $formCode .= $element->render();
        }

        return $formCode;
    }

    public function addElement(RenderableInterface $element)
    {
        $this->elements[] = $element;
    }
}
