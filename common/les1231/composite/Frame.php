<?php

namespace common\les1231\composite;

use common\les1231\composite\RenderableInterface as RenderableInterface;

/**
 * Class Frame
 *
 * @package common\les1231\composite
 */
class Frame implements RenderableInterface
{
    public function render(): string
    {
        return '<br>Frame';
    }
}
