<?php

namespace common\les1231\flyweight;

use common\les1231\flyweight\IceCream as IceCream;

/**
 * Class IceCream
 *
 * @package common\les1231\flyweight
 */
class IceCreamMaker
{
    protected $availableIceCream = [];

    public function make($preference)
    {
        if (empty($this->availableIceCream[$preference])) {
            $this->availableIceCream[$preference] = new IceCream();
        }

        return $this->availableIceCream[$preference];
    }
}
