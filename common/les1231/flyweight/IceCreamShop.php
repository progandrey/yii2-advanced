<?php

namespace common\les1231\flyweight;

use common\les1231\flyweight\IceCreamMaker as IceCreamMaker;

/**
 * Class IceCreamShop
 *
 * @package common\les1231\flyweight
 */
class IceCreamShop
{
    private $iceCream;
    private $orders = [];

    public function __construct(IceCreamMaker $iceCream)
    {
        $this->iceCream = $iceCream;
    }

    public function addOrder(int $user, int $iceCreamType): void
    {
        $this->orders[$user] = $this->iceCream->make($iceCreamType);
    }

    public function serve()
    {
        foreach ($this->orders as $user => $iceCream) {
            echo $iceCream;
        }
    }
}
