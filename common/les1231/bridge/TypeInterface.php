<?php

namespace common\les1231\bridge;

/**
 * Interface TypeInterface
 *
 * @package common\les1231\bridge
 */
interface TypeInterface
{
    public function getType();
}
