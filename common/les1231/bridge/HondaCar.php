<?php

namespace common\les1231\bridge;

use common\les1231\bridge\CarInterface as CarInterface;
use common\les1231\bridge\TypeInterface as TypeInterface;

/**
 * Class HondaCar
 *
 * @package common\les1231\bridge
 */
class HondaCar implements CarInterface
{
    protected $type;

    public function __construct(TypeInterface $type)
    {
        $this->type = $type;
    }

    public function getCar()
    {
        return "Car honda " . $this->type->getType();
    }
}
