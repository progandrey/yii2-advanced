<?php

namespace common\les1231\decorator;

use common\les1231\decorator\CarInterface as CarInterface;

/**
 * Class CarFromAirConditioner
 *
 * @package common\les1231\composite
 */
class CarFromAirConditioner implements CarInterface
{
    public $car;

    public function __construct(CarInterface $car)
    {
        $this->car = $car;
    }

    public function getCost()
    {
        return $this->car + 800;
    }

    public function getDescription()
    {
        return $this->getDescription() . ', кондицыонер';
    }
}
