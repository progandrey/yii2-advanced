<?php

namespace common\les1231\decorator;

use common\les1231\decorator\CarInterface as CarInterface;

/**
 * Class Car
 *
 * @package common\les1231\decorator
 */
class Car implements CarInterface
{
    public function getCost()
    {
        return 10000;
    }

    public function getDescription()
    {
        return 'Базовая комплектация';
    }
}
