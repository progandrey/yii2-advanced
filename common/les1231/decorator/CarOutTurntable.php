<?php

namespace common\les1231\decorator;

use common\les1231\decorator\CarInterface as CarInterface;

/**
 * Class CarOutTurntable
 *
 * @package common\les1231\composite
 */
class CarOutTurntable implements CarInterface
{
    public $car;

    public function __construct(CarInterface $car)
    {
        $this->car = $car;
    }

    public function getCost()
    {
        return $this->car + 200;
    }

    public function getDescription()
    {
        return $this->getDescription() . ', проигрыватель';
    }
}
