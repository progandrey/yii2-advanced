<?php

namespace common\les1231\facade;

/**
 * Class Car
 *
 * @package common\les1231\facade
 */
class Car
{
    public function brought()
    {
        echo "Бжик!!";
    }

    public function fuelSupply()
    {
        echo "джжж!";
    }

    public function engineStarting()
    {
        echo "Др Др Др. Завелись)";
    }

    public function turnedOff()
    {
        echo "дж бух бух!";
    }

    public function fuelCutOff()
    {
        echo "Машына заглохла!";
    }
}
