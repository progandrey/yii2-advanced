<?php

namespace common\les1231\facade;

use common\les1231\facade\Car as Car;

/**
 * Class CarFacade
 *
 * @package common\les1231\facade
 */
class CarFacade
{
    private $car;

    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    public function on()
    {
        $this->car->brought();
        $this->car->fuelSupply();
        $this->car->engineStarting();
    }

    public function off()
    {
        $this->car->turnedOff();
        $this->car->fuelCutOff();
    }
}
