<?php

namespace common\les1231\adaptor;

use common\les1231\adaptor\MonitorInterface as MonitorInterface;

/**
 * Class MonitorLG
 *
 * @package common\les1231\adaptor
 */
class MonitorLG implements MonitorInterface
{
    /**
     * @return bool
     */
    public function toPlugVGA(): bool
    {
        return true;
    }
}
