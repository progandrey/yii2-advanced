<?php

namespace common\les1231\adaptor;

/**
 * Interface MonitorInterface
 *
 * @package common\les1231\adaptor
 */
interface MonitorInterface
{
    public function toPlugVGA();
}
