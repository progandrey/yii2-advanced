<?php

namespace common\les1231\adaptor;

use common\les1231\adaptor\MonitorInterface as MonitorInterface;

/**
 * Class MonitorPhilips
 *
 * @package common\les1231\adaptor
 */
class MonitorPhilips implements MonitorInterface
{
    /**
     * @return bool
     */
    public function toPlugVGA(): bool
    {
        return true;
    }
}