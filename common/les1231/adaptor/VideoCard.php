<?php

namespace common\les1231\adaptor;

/**
 * Class VideoCard
 *
 * @package common\les1231\adaptor
 */
class VideoCard
{
    /**
     * @param MonitorInterface $monitor
     */
    public function toPlug(MonitorInterface $monitor)
	{
		$monitor->toPlugVGA();
	}
}
