<?php

namespace common\les1231\adaptor;

use common\les1231\adaptor\MonitorInterface as MonitorInterface;
use common\les1231\adaptor\MonitorSamsung as MonitorSamsung;

/**
 * Class MonitorAdaptor
 *
 * @package common\les1231\adaptor
 */
class MonitorAdaptor implements MonitorInterface
{
    private $samsung;

    public function __construct(MonitorSamsung $samsung)
    {
        $this->samsung = $samsung;
    }

    public function toPlugVGA()
    {
        $this->samsung->toPlugDVI();
    }
}
