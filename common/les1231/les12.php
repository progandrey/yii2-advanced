<?php

namespace common\les1231\Adaptor;

// 3.1
/*
use common\les1231\adaptor\MonitorSamsung as MonitorSamsung;
use common\les1231\adaptor\MonitorAdaptor as MonitorAdaptor;
use common\les1231\adaptor\VideoCard as VideoCard;

$monitorSamsung = new MonitorSamsung();
$monitorAdaptor = new MonitorAdaptor($monitorSamsung);

$videoCard = new VideoCard();
$videoCard->toPlug($monitorAdaptor);

// 3.2
use common\les1231\bridge\SedanType as SedanType;
use common\les1231\bridge\ToyotaCar as ToyotaCar;

$sedanType = new SedanType();

$car = new ToyotaCar($sedanType);
$car->getCar();

// 3.3
$car = new Car();
$car->addElement(new Frame());
$car->addElement(new Wheels());
$car->addElement(new Wheels());
$car->addElement(new Wheels());
$car->addElement(new Wheels());
$car->addElement(new Engine());
$form->render();

// 3.4
$car = new Car();
echo $car->getCost() . ' дол.';

$сarFromAirConditioner = new CarFromAirConditioner($car);
echo $сarFromAirConditioner->getCost() . ' дол.';

$сarOutTurntable = new CarOutTurntable($сarFromAirConditioner);
echo $сarOutTurntable->getCost() . ' дол.';

$сarFromAirConditioner = new CarFromAirConditioner(clone $car);
echo $сarFromAirConditioner->getCost() . ' дол.';

// 3.5
use common\les1231\facade\CarFacade as Car35;

$car35 = new CarFacade35();
$car35->on();
$car35->off();

// 3.6
use common\les1231\flyweight\IceCreamMaker as IceCreamMaker36;
use common\les1231\flyweight\IceCreamShop as IceCreamShop36;

$iceCream = new IceCreamMaker36();
$shop = new IceCreamShop36($iceCream);

$shop->addOrder(1, 2);
$shop->addOrder(2, 2);
$shop->addOrder(3, 3);
$shop->serve();
*/