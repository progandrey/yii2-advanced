<?php

namespace common\bootstrap;

use yii\base\BootstrapInterface;
use common\components\MailerInterface;
use common\components\LogFile;
use common\components\Mailer;
use yii\db\Connection;
use yii\web\UploadedFile;
Yii::setAlias('@LogMailDB', '@common/components/LogMailDB');

class SetUp implements BootstrapInterface
{
    public $app;
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app): void
    {
        $container = new \yii\di\Container;

        /** task 1.3 */
        $container->setSingleton(Mailer::class, Connection::class);

        /** task 1.4 */
        $container->set(LogFile::class, UploadedFile::class);

        /** task 1.5 */
        $container->set(\common\components\LogDB::class, Connection::class);

        /** task 1.6 */
        $container->set('@LogMailDB', Connection::class);

        /** task 1.7 */
        /*$container->set(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });*/

        /** task 1.8 */
        $container->set('yii\db\Connection', [
            'dsn' => 'pgsql:host=127.0.0.1;dbname=yii-advanced',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        /** task 1.9 */
        $container->set('db', [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        /** task 1.10 */
    }

}
