<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@LogMailDB' => '@common/components/LogMailDB'
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'advert' => [
            'class' => 'common\components\Adverts',
            'name' => 'Ivanov',
            'description' => 'This nice advert',
            'author' => 'Ya)',
            'date' => '25.05.2019 00:49',
            'active' => 'Y',
            'img' => 'petrov.gif',
        ],
        'article' => [
            'class' => 'common\components\Articles',
            'name' => 'Petrov',
            'description' => 'This nice article',
            'author' => 'Ya)',
            'date' => '25.07.2019 00:49',
            'active' => 'Y',
        ],
        'factory' => [
            'class' => 'common\components\Factory',
        ],
        'definitions' => [
            'common\components\LogFile',
            '\common\components\LogDB',
            '@LogMailDB',
            'yii\db\Connection', [
                'dsn' => 'pgsql:host=127.0.0.1;dbname=yii-advanced',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
            ],
            'db', [
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=127.0.0.1;dbname=demo',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
            ]
        ],
        'singletons' => [
            'common\components\Mailer'
        // Конфигурация для единожды создающихся объектов
        ],
        /*'privatehouse' => [
            'class' => 'common\components\PrivateHouse',
        ],
        'apartmenthouse' => [
            'class' => 'common\components\Apartmenthouse',
        ],*/
    ],
    'bootstrap' => [
        'common\bootstrap\SetUp',
    ],
];
