<?php

namespace common\pattern\Prototype;

/**
 * Class Sedan
 *
 * @package common\pattern\Prototype
 */
class Sedan extends Car
{
    /**
     * Sedan constructor.
     *
     * @param string $name
     * @param string $mark
     */
    public function __construct(string $name, string $mark)
    {
        $this->name = $name;
        $this->mark = $mark;
    }

    /**
     * Clone and edit params mark
     */
    public function __clone()
    {
        $this->mark = 'renault';
    }
}