<?php

namespace common\pattern\Prototype;

/**
 * Class Car
 *
 * @package common\pattern\Prototype
 */
abstract class Car
{
    protected $name;
    protected $mark;

    abstract public function __clone();

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMark(): string
    {
        return $this->mark;
    }
}