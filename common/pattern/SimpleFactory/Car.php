<?php

namespace common\pattern\SimpleFactory;

/**
 * Interface Car
 *
 * @package pattern\SimpleFactory
 */
interface Car
{
    /**
     * @return float
     */
    public function getWidth(): float;

    /**
     * @return float
     */
    public function getHeight(): float;

    /**
     * @return string
     */
    public function getColor(): string ;

    /**
     * @return float
     */
    public function getWeight(): float ;
}
