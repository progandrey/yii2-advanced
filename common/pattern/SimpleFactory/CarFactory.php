<?php

namespace common\pattern\SimpleFactory;

/**
 * Class CarFactory
 *
 * @package common\pattern\SimpleFactory
 */
class CarFactory
{
    /**
     * Return create car
     *
     * @param float $height
     * @param float $width
     * @param string $color
     * @param float $weight
     * @param string $type
     *
     * @return Car
     */
    public static function create(float $height, float $width, string $color, float $weight, string $type): Car
    {
        if ('sedan' === $type) {
            return new SedanCar($height, $width, $color, $weight);
        }
        return new HatchbackCar($height, $width, $color, $weight);
    }
}
