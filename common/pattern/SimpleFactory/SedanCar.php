<?php

namespace common\pattern\SimpleFactory;

/**
 * Class SedanCar
 * @package common\pattern\SimpleFactory
 */
class SedanCar implements Car
{
    public $height;
    public $width;
    public $color;
    public $weight;

    /**
     * SedanCar constructor.
     *
     * @param float $height
     * @param float $width
     * @param string $color
     * @param float $weight
     */
    public function __construct(float $height, float $width, string $color, float $weight)
    {
        $this->height = $height;
        $this->width = $width;
        $this->color = $color;
        $this->weight = $weight;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}
