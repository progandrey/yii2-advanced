<?php

namespace common\pattern\FabricMethod;

/**
 * Class RepairerFactory
 *
 * @package common\pattern\FabricMethod
 */
class RepairerFactory extends Manager
{
    /**
     * @return Work
     */
    public function askQuestions(): Work
    {
        return new Repairer();
    }
}


