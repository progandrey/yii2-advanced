<?php

namespace common\pattern\FabricMethod;

/**
 * Class SallerFactory
 *
 * @package common\pattern\FabricMethod
 */
class SallerFactory extends Manager
{
    /**
     * @return Work
     */
    public function askQuestions(): Work
    {
        return new Saller();
    }
}
