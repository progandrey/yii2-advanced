<?php

namespace common\pattern\FabricMethod;

/**
 * Class Saller
 *
 * @package common\pattern\FabricMethod
 */
class Saller implements Work
{
    /**
     * @return string
     */
    public function inspectionCar(): string
    {
        return 'saller car';
    }
}