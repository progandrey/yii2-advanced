<?php

namespace common\pattern\FabricMethod;

/**
 * Interface Work
 *
 * @package common\pattern\FabricMethod
 */
interface Work
{
    public function inspectionCar();
}