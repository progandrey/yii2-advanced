<?php

namespace common\pattern\FabricMethod;

/**
 * Class Manager
 *
 * @package common\pattern\FabricMethod
 */
abstract class Manager
{
    /**
     * Фабричный метод
     *
     * @return Work
     */
    abstract public function askQuestions(): Work;

    public function customerRequest()
    {
        $appeal = $this->askQuestions();
        $appeal->inspectionCar();
    }
}