<?php

namespace common\pattern\FabricMethod;

/**
 * Class Repairer
 *
 * @package common\pattern\FabricMethod
 */
class Repairer implements Work
{
    /**
     * @return string
     */
    public function inspectionCar()
    {
        return 'repairer car';
    }
}