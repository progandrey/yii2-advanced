<?php

namespace common\pattern\Builder;

/**
 * Class PizzaBuilder
 *
 * @package common\pattern\Builder
 *
 * @params bool $yeast
 * @params bool $flour
 * @params bool $salt
 * @params bool $tomato
 * @params bool $cheese
 * @params bool $ham
 * @params bool $onion
 * @params bool $mushrooms
 * @params bool $olives
 */
class PizzaBuilder
{
    public $yeast = false;
    public $flour = false;
    public $salt = false;
    public $tomato = false;
    public $cheese = false;
    public $ham = false;
    public $onion = false;
    public $mushrooms = false;
    public $olives = false;

    /**
     * @param bool $yeast
     *
     * @return object
     */
    public function setYeast(bool $yeast): object
    {
        $this->yeast = $yeast;
        return $this;
    }

    /**
     * @param bool $flour
     *
     * @return object
     */
    public function setFlour(bool $flour): object
    {
        $this->flour = $flour;
        return $this;
    }

    /**
     * @param bool $salt
     *
     * @return object
     */
    public function setSalt(bool $salt): object
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @param bool $tomato
     *
     * @return object
     */
    public function setTomato(bool $tomato): object
    {
        $this->tomato = $tomato;
        return $this;
    }

    /**
     * @param bool $cheese
     *
     * @return object
     */
    public function setCheese(bool $cheese): object
    {
        $this->$cheese = $cheese;
        return $this;
    }

    /**
     * @param bool $ham
     *
     * @return object
     */
    public function setHam(bool $ham): object
    {
        $this->ham = $ham;
        return $this;
    }

    /**
     * @param bool $onion
     *
     * @return object
     */
    public function setOnion(bool $onion): object
    {
        $this->onion = $onion;
        return $this;
    }

    /**
     * @param bool $mushrooms
     *
     * @return object
     */
    public function setMushrooms(bool $mushrooms): object
    {
        $this->mushrooms = $mushrooms;
        return $this;
    }

    /**
     * @param bool $olives
     *
     * @return object
     */
    public function setOlives(bool $olives): object
    {
        $this->olives = $olives;
        return $this;
    }

    /**
     * @return Pizza
     */
    public function build(): Pizza
    {
        return new Pizza($this);
    }
}
