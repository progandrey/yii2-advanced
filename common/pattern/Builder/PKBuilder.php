<?php

namespace common\pattern\Builder;

/**
 * Class PKBuilder
 *
 * @package common\pattern\Builder
 *
 * @params bool $block
 * @params bool $nutrition
 * @params bool $motherboard
 * @params bool $processor
 * @params bool $memory
 * @params bool $disk
 */
class PKBuilder
{
    public $block = false;
    public $nutrition = false;
    public $motherboard = false;
    public $processor = false;
    public $memory = false;
    public $disk = false;

    /**
     * @param bool $block
     *
     * @return object
     */
    public function setBlock(bool $block): object
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @param bool $nutrition
     *
     * @return object
     */
    public function setNutrition(bool $nutrition): object
    {
        $this->nutrition = $nutrition;
        return $this;
    }

    /**
     * @param bool $motherboard
     *
     * @return object
     */
    public function setMotherboard(bool $motherboard): object
    {
        $this->motherboard = $motherboard;
        return $this;
    }

    /**
     * @param bool $processor
     *
     * @return object
     */
    public function setProcessor(bool $processor): object
    {
        $this->processor = $processor;
        return $this;
    }

    /**
     * @param bool $memory
     *
     * @return object
     */
    public function setMemory(bool $memory): object
    {
        $this->memory = $memory;
        return $this;
    }

    /**
     * @param bool $disk
     *
     * @return object
     */
    public function setDisk(bool $disk): object
    {
        $this->disk = $disk;
        return $this;
    }

    /**
     * @return PK
     */
    public function build(): PK
    {
        return new PK($this);
    }
}
