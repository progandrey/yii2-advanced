<?php

namespace common\pattern\Builder;

/**
 * Class Car
 *
 * @package common\pattern\Builder
 *
 * @params bool $body
 * @params string $color
 * @params int $doors
 * @params int $wheels
 * @params float $engine
 */
class Car
{
    public $body = false;
    public $color;
    public $doors;
    public $wheels;
    public $engine;

    /**
     * Car constructor.
     *
     * @param CarBuilder $builder
     */
    public function __construct(CarBuilder $builder)
    {
        $this->body = $builder->body;
        $this->color = $builder->color;
        $this->doors = $builder->doors;
        $this->wheels = $builder->wheels;
        $this->engine = $builder->engine;
    }
}
