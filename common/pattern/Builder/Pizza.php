<?php

namespace common\pattern\Builder;

/**
 * Class Pizza
 *
 * @package common\pattern\Builder
 *
 * @params bool $yeast
 * @params bool $flour
 * @params bool $salt
 * @params bool $tomato
 * @params bool $cheese
 * @params bool $ham
 * @params bool $onion
 * @params bool $mushrooms
 * @params bool $olives
 */
class Pizza
{
    public $yeast = false;
    public $flour = false;
    public $salt = false;
    public $tomato = false;
    public $cheese = false;
    public $ham = false;
    public $onion = false;
    public $mushrooms = false;
    public $olives = false;

    /**
     * Pizza constructor.
     *
     * @param PizzaBuilder $builder
     */
    public function __construct(PizzaBuilder $builder)
    {
        $this->yeast = $builder->yeast;
        $this->flour = $builder->flour;
        $this->salt = $builder->salt;
        $this->tomato = $builder->tomato;
        $this->cheese = $builder->cheese;
        $this->ham = $builder->ham;
        $this->onion = $builder->onion;
        $this->mushrooms = $builder->mushrooms;
        $this->olives = $builder->olives;
    }
}
