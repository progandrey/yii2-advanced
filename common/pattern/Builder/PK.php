<?php

namespace common\pattern\Builder;

/**
 * Class PK
 *
 * @package common\pattern\Builder
 *
 * @params bool $block
 * @params bool $nutrition
 * @params bool $motherboard
 * @params bool $processor
 * @params bool $memory
 * @params bool $disk
 */
class PK
{
    public $block = false;
    public $nutrition = false;
    public $motherboard = false;
    public $processor = false;
    public $memory = false;
    public $disk = false;

    /**
     * PK constructor.
     *
     * @param PKBuilder $builder
     */
    public function __construct(PKBuilder $builder)
    {
        $this->block = $builder->block;
        $this->nutrition = $builder->nutrition;
        $this->motherboard = $builder->motherboard;
        $this->processor = $builder->processor;
        $this->memory = $builder->memory;
        $this->disk = $builder->disk;
    }
}
