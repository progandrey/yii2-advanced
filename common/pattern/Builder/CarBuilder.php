<?php

namespace common\pattern\Builder;

/**
 * Class CarBuilder
 *
 * @package common\pattern\Builder
 *
 * @params bool $body
 * @params string $color
 * @params int $doors
 * @params int $wheels
 * @params float $engine
 */
class CarBuilder
{
    public $body = false;
    public $color;
    public $doors;
    public $wheels;
    public $engine;

    /**
     * @param bool $body
     *
     * @return object
     */
    public function setBody(bool $body): object
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $color
     *
     * @return object
     */
    public function setColor(string $color): object
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @param int $doors
     *
     * @return object
     */
    public function setDoors(int $doors): object
    {
        $this->doors = $doors;
        return $this;
    }

    /**
     * @param int $wheels
     *
     * @return object
     */
    public function setWheels(int $wheels): object
    {
        $this->wheels = $wheels;
        return $this;
    }


    /**
     * @param float $engine
     *
     * @return object
     */
    public function setEngine(float $engine): object
    {
        $this->engine = $engine;
        return $this;
    }

    /**
     * @return Car
     */
    public function build(): Car
    {
        return new Car($this);
    }
}
