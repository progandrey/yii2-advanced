<?php

namespace common\pattern\Singleton;

/**
 * Class Director
 *
 * @package common\pattern\Singleton
 */
final class Director
{
    private static $instance;

    /**
     * Off constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Director
     */
    public static function getInstance(): Director
    {
        if (! self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Off Clone
     */
    private function __clone()
    {
    }

    /**
     * Off wakeup
     */
    private function __wakeup()
    {
    }
}