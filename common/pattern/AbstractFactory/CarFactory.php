<?php

namespace common\pattern\AbstractFactory;

/**
 * Interface CarFactory
 *
 * @package common\pattern\AbstractFactory
 */
interface CarFactory
{
    public function produceSidan();
    public function produceTruck();
}