<?php

namespace common\pattern\AbstractFactory;

/**
 * Class ClassicCarFactory
 *
 * @package common\pattern\AbstractFactory
 */
class ClassicCarFactory implements CarFactory
{
    /**
     * @return Sidan
     */
    public function produceSidan()
    {
        return new Sidan();
    }

    /**
     * @return Truck
     */
    public function produceTruck()
    {
        return new Truck();
    }
}
