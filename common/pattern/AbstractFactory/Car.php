<?php

namespace common\pattern\AbstractFactory;

/**
 * Interface Car
 *
 * @package common\pattern\AbstractFactory
 */
interface Car
{
    public function hasWheels();
}