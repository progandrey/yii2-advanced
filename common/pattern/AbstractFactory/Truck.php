<?php

namespace common\pattern\AbstractFactory;

/**
 * Class Truck
 *
 * @package common\pattern\AbstractFactory
 */
class Truck implements Car
{
    /**
     * @return int
     */
    public function hasWheels(): int
    {
        return 6;
    }
}