<?php

namespace common\pattern\AbstractFactory;

/**
 * Class Sidan
 *
 * @package common\pattern\AbstractFactory
 */
class Sidan implements Car
{
    /**
     * @return int
     */
    public function hasWheels(): int
    {
        return 4;
    }
}