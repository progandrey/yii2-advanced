<?php

namespace common\pattern\AbstractFactory;

/**
 * Class ModernCarFactory
 *
 * @package common\pattern\AbstractFactory
 */
class ModernCarFactory implements CarFactory
{
    /**
     * @return Sidan
     */
    public function produceSidan()
    {
        return new Sidan();
    }

    /**
     * @return Truck
     */
    public function produceTruck()
    {
        return new Truck();
    }
}
