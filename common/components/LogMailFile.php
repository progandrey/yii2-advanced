<?php

namespace common\components;

use yii\web\UploadedFile;

/**
 * Class LogMailFile
 *
 * @package common\components
 */
class LogMailFile
{
    public $file;

    public function __construct(UploadedFile $file)
    {
        return __CLASS__;
    }
}
