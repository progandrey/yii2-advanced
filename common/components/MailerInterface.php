<?php

namespace common\components;

use yii\db\Connection;

/**
 * Interface MailerInterface
 *
 * @package common\components
 */
interface MailerInterface
{
    public function mailer(): object ;
}
