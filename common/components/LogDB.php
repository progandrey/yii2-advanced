<?php

namespace common\components;

use yii\db\Connection;

/**
 * Class LogDB
 *
 * @package common\components
 */
class LogDB
{
    public function __construct(Connection $db)
    {
        return __CLASS__;
    }
}
