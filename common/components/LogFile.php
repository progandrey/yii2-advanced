<?php

namespace common\components;

use yii\web\UploadedFile;

/**
 * Class LogFile
 *
 * @package common\components
 */
class LogFile
{
    public function __construct(UploadedFile $file)
    {
        return __CLASS__;
    }
}
