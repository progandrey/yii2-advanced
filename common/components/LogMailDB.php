<?php

namespace common\components;

use yii\db\Connection;

/**
 * Class LogMailDB
 *
 * @package common\components
 */
class LogMailDB
{
    public function __construct(Connection $db)
    {
        return __CLASS__;
    }
}
