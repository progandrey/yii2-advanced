<?php

namespace common\components;

use common\pattern\Builder\CarBuilder;
use common\pattern\Builder\PizzaBuilder;
use common\pattern\Builder\PKBuilder;
use common\pattern\FabricMethod\SallerFactory;
use common\pattern\SimpleFactory\CarFactory;
use common\pattern\AbstractFactory\ClassicCarFactory;
use common\pattern\AbstractFactory\ModernCarFactory;
use common\pattern\Prototype\Sedan;
use common\pattern\Singleton\Director;

/**
 * Class Factory
 *
 * @package common\components
 *
 * @params string $name
 * @params string $description
 * @params string $author
 * @params string $date
 * @params string $active
 * @params string $img
 */
class Factory
{
    /**
     * @return object
     */
    public function getSimpleFactory(): object
    {
        $car = CarFactory::create(180, 400, 'black', 1800, 'sedan');

        $car->getHeight();
        $car->getColor();
        return $car;
    }

    /**
     * @return object
     */
    public function getBuilder(): object
    {
        $builder = new CarBuilder();

        $builder->setBody(true);
        $builder->setColor('black');
        $builder->setDoors(4);
        $builder->setWheels(4);
        $builder->setEngine(1.6);

        return $builder->build();
    }

    /**
     * @return string
     */
    public function getFabricMethod(): string
    {

        $sallerFactory = new SallerFactory();

        $saller = $sallerFactory->askQuestions();

        return $saller->inspectionCar();
    }

    /**
     * @return int
     */
    public function getAbstractFactory(): int
    {
        $classic = new ClassicCarFactory();

        $truckClassic = $classic->produceTruck();


        $sidanClassic = $classic->produceSidan();
        $sidanClassic->hasWheels();


        $factoryForHipster = new ModernCarFactory();

        $sofaForHipster = $factoryForHipster->produceTruck();
        $sofaForHipster->hasWheels();

        $chairForHipster = $factoryForHipster->produceSidan();
        $chairForHipster->hasWheels();

        return $truckClassic->hasWheels();
    }

    /**
     * @return array
     */
    public function getPrototype(): array
    {
        $return = [];
        $original = new Sedan('corolla', 'toyota');
        $return['original']['name'] = $original->getName();
        $return['original']['mark'] = $original->getMark();

        $cloned = clone $original;
        $cloned->setName('Sandero');

        $return['cloned']['name'] =  $cloned->getName();
        $return['cloned']['mark'] =  $cloned->getMark();

        return $return;
    }

    /**
     * @return string
     */
    public function getSingleton(): string
    {
        if(Director::getInstance()) {
            return 'true';
        }

        return 'false';
    }

    /**
     * @return object
     */
    public function getBuilderPizza(): object
    {
        $builder = new PizzaBuilder();

        $builder->setYeast(true);
        $builder->setFlour(false);
        $builder->setSalt(false);
        $builder->setTomato(true);
        $builder->setCheese(true);
        $builder->setHam(true);
        $builder->setOnion(true);
        $builder->setMushrooms(true);
        $builder->setOlives(false);

        return $builder->build();
    }

    /**
     * @return object
     */
    public function getBuilderPK(): object
    {
        $builder = new PKBuilder();

        $builder->setBlock(true);
        $builder->setNutrition(true);
        $builder->setMotherboard(true);
        $builder->setProcessor(true);
        $builder->setMemory(true);
        $builder->setDisk(true);

        return $builder->build();
    }
}
