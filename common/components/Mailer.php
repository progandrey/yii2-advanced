<?php

namespace common\components;

use common\components\MailerInterface as MailerInterface;
use yii\db\Connection;

/**
 * Class Mailer
 *
 * @package common\components
 */
class Mailer implements MailerInterface
{
    public $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function mailer(): object
    {
        return __CLASS__;
    }
}
