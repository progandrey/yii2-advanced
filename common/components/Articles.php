<?php

namespace common\components;

use common\models\Article;
use common\models\User;

/**
 * Class Article
 *
 * @package common\components
 *
 * @params string $name
 * @params string $description
 * @params string $author
 * @params string $date
 * @params string $active
 */
class Articles
{
    public $name;
    public $description;
    public $author;
    public $date;
    public $active;

    /**
     * Articles constructor.
     *
     * @param string $name
     * @param string $description
     * @param string $author
     * @param string $date
     * @param string $active
     */
    /*public function __construct(string $name, string $description, string $author, string $date, string $active)
    {
        $this->name = $name;
        $this->description = $description;
        $this->author = $author;
        $this->date = $date;
        $this->active = $active;
    }*/

    /**
     * Return object params $this
     *
     * @return object
     */
    public function getParams(): object
    {
        return $this;
    }

    public function getFIO(): string
    {
        $user = Article::findOne([
            "username" => 'Vasya',
        ]);

        $user2 = User::find()
            ->where([
                "username" => 'Vasya',
            ])
            ->andWhere()
            ->orderBy()
            ->limit()
            ->join();

    }

    public function getUserByName(string $name): object
    {
        $user = User::findOne([
            "username" => 'Vasya',
        ]);

        $user2 = User::find()
            ->where([
                "username" => 'Vasya',
            ])
            ->andWhere()
            ->orderBy()
            ->limit()
            ->join();

    }

    public function set(): bool
    {
        /*$values = [
            'name' => 'James',
            'email' => 'james@example.com',
        ];*/

        $customer = new Article();

        $customer->attributes = $this;
        return $customer->save();
    }
}
