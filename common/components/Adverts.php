<?php

namespace common\components;

/**
 * Class Advert
 *
 * @package common\components
 *
 * @params string $name
 * @params string $description
 * @params string $author
 * @params string $date
 * @params string $active
 * @params string $img
 */
class Adverts
{
    public $name;
    public $description;
    public $author;
    public $date;
    public $active;
    public $img;

    /**
     * Articles constructor.
     *
     * @param string $name
     * @param string $description
     * @param string $author
     * @param string $date
     * @param string $active
     * @param string $img
     */
    public function __construct(string $name, string $description, string $author, string $date, string $active, string $img)
    {
        $this->name = $name;
        $this->description = $description;
        $this->author = $author;
        $this->date = $date;
        $this->active = $active;
        $this->img = $img;
    }

    /**
     * Return object params $this
     *
     * @return object
     */
    public function getParams(): object
    {
        return $this;
    }
}
