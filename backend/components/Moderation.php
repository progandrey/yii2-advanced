<?php

namespace backend\components;

/**
 * Class Moderation
 *
 * @package backend\components
 *
 * @param bool $activate
 */
class Moderation
{
    public $activate;

    /**
     * Set activate in declare
     *
     * @param bool $activate
     */
    public function setActivate(bool $activate)
    {
        $this->activate = $activate;
    }

    /**
     * Get activate in declare
     *
     * @return bool
     */
    public function getBall(): bool
    {
        return $this->activate;
    }
}
