<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=127.0.0.1;dbname=yii2advanced',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'article' => [
            'class' => 'common\components\Articles',
            'name' => 'Petrov',
            'description' => 'This nice article DEV Common',
            'author' => 'Ya)',
            'date' => '25.07.2019 00:49',
            'active' => 'Y',
        ],
    ],
];
