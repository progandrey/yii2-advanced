<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'article' => [
            'class' => 'common\components\Articles',
            'name' => 'Petrov',
            'description' => 'This nice article Prod Front',
            'author' => 'Ya)',
            'date' => '25.07.2019 00:49',
            'active' => 'Y',
        ],
    ],
];
