<?php

namespace frontend\components;

use common\models\Advert;
use common\models\Article;
use common\models\User;
use yii\base\Component;
use yii\db\Transaction;
use Yii;

/**
 * Class Test
 *
 * @package frontend\components
 */
class Test extends Component
{
    /**
     * Task #1
     * Return arrey in table category
     *
     * @return array
     *
     * @throws \yii\db\Exception
     */
    public function getTask1(): array
    {
        $array = Yii::$app->db->createCommand("SELECT id || ' ' || name FROM category UNION SELECT id || ' ' || name FROM sub_category")
            ->queryAll();

        return $array;
    }

    public function getTask22(): array
    {
        $array = Yii::$app->db->createCommand("SELECT name FROM countries UNION SELECT name FROM cities")
            ->queryAll();

        return $array;
    }

    /**
     * Task #4
     * Return array in tables
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getTask4(): array
    {
        $array = Yii::$app->db->createCommand('
SELECT c.id c_id, c.name c_name, p.id p_id, p.name p_name, p.category_id p_category_id, sc.id sc_id, sc.name sc_name, sc.category_id sc_category_id
FROM category c, page p, sub_category sc 
WHERE c.id = p.category_id')
            ->queryAll();

        return $array;
    }

    /**
     * transaction Create Table
     *
     * @throws \Exception
     */
    public function transactionCreateTable()
    {
        $sql1 = "CREATE TABLE films (id integer PRIMARY KEY, title varchar(40) NOT NULL, len integer);";
        $sql2 = "INSERT INTO films (id, title, len) VALUES (1, 'Wars', 223), (2, 'Titanik', 156), (3, 'Forsage', 112)";

        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $db->createCommand($sql1)->execute();
            $db->createCommand($sql2)->execute();

            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
        }
    }

    /**
     * transaction Drop Table
     *
     * @throws \yii\db\Exception
     */
    public function transactionDropTable()
    {
        $db = Yii::$app->db;
        $transaction = User::getDb()->beginTransaction();
        if ($db->createCommand("UPDATE films SET title = 'Dramatic' WHERE id = 2")->execute()) {
            $db->createCommand("DROP TABLE films;")->execute();
            $transaction->commit();
        } else {
            $transaction->rollBack();
            Yii::error("Errors has been occured during transaction process", "transaction-error");
        }
    }
}
