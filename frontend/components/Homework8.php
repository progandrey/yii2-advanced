<?php

namespace frontend\components;

use common\models\Advert;
use common\models\Article;
use common\models\User;
use yii\base\Component;
use yii\db\Transaction;
use Yii;

/**
 * Class Test
 *
 * @package frontend\components
 */
class Homework8 extends Component
{
    public $article;

    public function getSelectArticle1()
    {
        $this->article = Article::find()
            ->where(['id' => 1])
            ->one();

        return $this->article;
    }

    public function getSelectArticle2()
    {
        $this->article = Article::find()
            ->asArray()
            ->all();

        return $this->article;
    }

    public function getSelectArticle3()
    {
        foreach (Article::find()->batch(10) as $article3) {
            echo $this->article['name'];
        }

        return $this->article;
    }

    public function getSelectArticle4()
    {
        return $article4 = Article::findAll([1, 1000000]);
    }

    public function getSelectArticle5()
    {
        return Article::findAll([
            'author' => 'Ivan',
        ]);
    }

    public function getSelectArticle6()
    {
        $this->article = Advert::find()
            ->where(['id' => 1])
            ->one();

        return $this->article;
    }


    public function getSelectArticle7()
    {
        $this->article = Advert::find()
            ->asArray()
            ->all();

        return $this->article;
    }

    public function getSelectArticle8()
    {
        foreach (Advert::find()->each(10) as $advert3) {
            $this->article .= '<br>' . $advert3['name'];
        }

        return $this->article;
    }

    public function getSelectArticle9()
    {
        return Advert::findAll([10, 25, 50, 100]);
    }


    public function getSelectArticle10()
    {
        $this->article = Advert::findAll([
            'active' => 'Y',
        ]);

        return $this->article;
    }

    /**
     * Set Usert
     */
    public function setUser()
    {
        $array = [
            'username'=>'test11',
            'auth_key'=>'fghfghfghfghf',
            'password_hash'=>'sdfsfasf af adf adf adf',
            'password_reset_token'=>'523d23d25dtgfgdtwerf',
            'email'=>'test@test.ru',
            'status' => '1',
            'created_at' => '1',
            'updated_at' => '1',
        ];

        $use = new User();

        $use->attributes = $array;
        $use->save();
    }

    /**
     * Set Articles in yii
     */
    public function setArticle()
    {
        $article = new Article();
        $article->name = 'article in components';
        $article->description = 'tra la la';
        $article->author = 'James';
        $article->date = '24/07/2019';
        $article->active = 'Y';
        $article->save();
    }

    /**
     * Set Advert site
     */
    public function setAdvert()
    {
        $advert = new Advert();
        $advert->name = 'article in components';
        $advert->description = 'tra la la';
        $advert->author = 'James';
        $advert->date = '24/07/2019';
        $advert->active = 'Y';
        $advert->img = 'img';
        $advert->save();
    }

    /**
     * Delete articles by $name
     *
     * @param string $name
     */
    public function deleteArticleByName(string $name)
    {
        Article::deleteAll(['name' => 'Old life in New York']);
    }

    /**
     * Delete article by ID
     *
     * @param int $id
     */
    public function deleteArticleById(int $id)
    {
        $article = Article::findOne($id);
        $article->delete();
    }

    /**
     * update Article By Id
     *
     * @param int $id
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateArticleById(int $id)
    {
        $article = Article::findOne($id);
        $article->date = '22/07/2019';
        $article->active = 'N';
        $article->update();
    }

    /**
     * update Advert By Id
     *
     * @param int $id
     *
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateAdvertById(int $id)
    {
        $article = Article::findOne($id);
        $article->date = '21/07/2017';
        $article->active = 'N';
        $article->img = 'pupkin.jpg';
        $article->update();
    }
}
