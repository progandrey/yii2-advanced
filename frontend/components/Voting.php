<?php

namespace frontend\components;

/**
 * Class Voting
 *
 * @package frontend\components
 *
 * @param int $ball
 */
class Voting
{
    public $ball;

    /**
     * Set user ball in article or declare
     *
     * @param int $ball
     */
    public function setBall(int $ball)
    {
        $this->ball = $ball;
    }

    /**
     * Get ball in article or declare
     *
     * @return int
     */
    public function getBall(): int
    {
        return $this->ball;
    }
}
