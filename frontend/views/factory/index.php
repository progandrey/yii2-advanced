<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Home Work #10 Factory</h1>


        <div>Task # 1</div>
        <div><b>Simple Factory</b><br><br>Car params:
            <?php
            foreach ($task1011 AS $key => $value) {
                echo '<br>' . $key . ' ' . $value;
            }
            ?>
        </div>

        <div><br><b>Builder</b><br><br>Car:
            <?php
            foreach ($task1012 AS $key2 => $value2) {
                echo '<br>' . $key2 . ' ' . $value2;
            }
            ?>
        </div>

        <div><br><b>Fabric Method</b><br><br>Saloon car:
            <?= $task1013; ?>
        </div>

        <div><br><b>Abstract Factory</b><br><br>Car:
            wheels <?= $task1014; ?>
        </div>

        <div><br><b>Abstract Factory</b><br><br>Car:
            <?php
            foreach ($task1015 AS $key5 => $value5) {
                echo '<br>' . $key5 . ' name:  ' . $value5['name'] . ', mark: ' . $value5['mark'];
            }
            ?>
        </div>

        <div>
            <br><b>Singleton</b>
            <br><?= $task1016; ?>
        </div>

        <div><br><b>2.3 Pizza</b><br><br>Car:
            <?php
            foreach ($task1023 AS $key6 => $value6) {
                echo '<br>' . $key6 . ' ' . $value6;
            }
            ?>
        </div>

        <div><br><b>2.6 Build Car</b><br>
            <?php
            foreach ($task1026 AS $key7 => $value7) {
                echo '<br>' . $key7 . ' ' . $value7;
            }
            ?>
        </div>
    </div>
</div>
