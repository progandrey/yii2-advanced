<?php

/* @var $this yii\web\View */

$this->title = 'Home Work #9';
//echo '<pre>';print_r($task22);echo '</pre>';
//die();
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Home Work #9</h1>
        <div>
            <table border="1">
                <caption><b>Task #1 Table category</b></caption>
                <tr>
                    <th>name</th>
                </tr>
                <?php
                foreach ($task1 AS $_task1) {
                    echo '<tr>';
                    echo '<td>' . $_task1['?column?'] . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>

        <div>
            <table border="1">
                <caption><b>Task #2.2 Table countries</b></caption>
                <tr>
                    <th>name</th>
                </tr>
                <?php
                foreach ($task22 AS $_task22) {
                    echo '<tr>';
                    echo '<td>' . $_task22['name'] . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>

        <div>
            <table border="1">
                <caption><b>Task #4</b></caption>
                <tr>
                    <th>c_id</th>
                    <th>c_name</th>
                    <th>p_id</th>
                    <th>p_name</th>
                    <th>p_category_id</th>
                    <th>sc_id</th>
                    <th>sc_name</th>
                    <th>sc_category_id</th>
                </tr>
                <?php
                foreach ($task4 AS $_task4) {
                    echo '<tr>';
                    echo '<td>' . $_task4['c_id'] . '</td>';
                    echo '<td>' . $_task4['c_name'] . '</td>';
                    echo '<td>' . $_task4['p_id'] . '</td>';
                    echo '<td>' . $_task4['p_name'] . '</td>';
                    echo '<td>' . $_task4['p_category_id'] . '</td>';
                    echo '<td>' . $_task4['sc_id'] . '</td>';
                    echo '<td>' . $_task4['sc_name'] . '</td>';
                    echo '<td>' . $_task4['sc_category_id'] . '</td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>

        <div><?= $task61 ?></div>
        <div><?= $task62 ?></div>
    </div>
</div>
