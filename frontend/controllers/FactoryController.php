<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class FactoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $task1011 = Yii::$app->factory->getSimpleFactory();
        $task1012 = Yii::$app->factory->getBuilderPK();
        $task1013 = Yii::$app->factory->getFabricMethod();
        $task1014 = Yii::$app->factory->getAbstractFactory();
        $task1015 = Yii::$app->factory->getPrototype();
        $task1016 = Yii::$app->factory->getSingleton();
        $task1023 = Yii::$app->factory->getBuilderPizza();
        $task1026 = Yii::$app->factory->getBuilder();

        return $this->render('index', [
            'task1011' => $task1011,
            'task1012' => $task1012,
            'task1013' => $task1013,
            'task1014' => $task1014,
            'task1015' => $task1015,
            'task1016' => $task1016,
            'task1023' => $task1023,
            'task1026' => $task1026,
        ]);
    }
}
