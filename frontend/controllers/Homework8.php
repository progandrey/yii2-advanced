<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class Homework8Controller extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $task71 = Yii::$app->test->getSelectArticle1();
        $task72 = Yii::$app->test->getSelectArticle2();
        $task73 = Yii::$app->test->getSelectArticle3();
        $task74 = Yii::$app->test->getSelectArticle4();
        $task75 = Yii::$app->test->getSelectArticle5();
        $task76 = Yii::$app->test->getSelectArticle6();
        $task77 = Yii::$app->test->getSelectArticle7();
        $task78 = Yii::$app->test->getSelectArticle8();
        $task79 = Yii::$app->test->getSelectArticle9();
        $task710 = Yii::$app->test->getSelectArticle10();

        return $this->render('index', [
            'task71' => $task71,
            'task72' => $task72,
            'task73' => $task73,
            'task74' => $task74,
            'task75' => $task75,
            'task76' => $task76,
            'task77' => $task77,
            'task78' => $task78,
            'task79' => $task79,
            'task710' => $task710,
        ]);
    }
}
